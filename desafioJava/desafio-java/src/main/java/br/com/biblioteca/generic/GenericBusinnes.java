package br.com.biblioteca.generic;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Log4j2
public abstract class GenericBusinnes<
        T1 extends JpaRepository
        , DTOSaida extends GenericSaidaDTO
        , DTOEntrada extends GenericEntradaDTO
        , Entity extends GenericDomain> {

    protected DTOSaida modelo;

    @Autowired
    protected T1 modeloRepository;


    @Transactional
    public synchronized DTOSaida save(DTOEntrada entity) {

        log.info("<< Iniciar processo de salvar generico >>");

        try {
            Entity entrada = (Entity) entity.from();
            entrada = (Entity) modeloRepository.save(entrada);
            return (DTOSaida) entrada.from();
        } catch (Exception e) {
            log.info("Erro ao salvar dados : {}", e.toString());
        }
        return null;

    }


    public List<DTOSaida> getAll() {

        log.info("<< Retornar todos os registros >>");

        List<DTOSaida> entidades = new ArrayList<>();
        List<Entity> e = modeloRepository.findAll();

        for (Entity entity : e) {
            entidades.add((DTOSaida) entity.from());
        }

        log.info("<< Registros retornados => {} >>", e.size());

        return entidades;
    }


    public DTOSaida getID(Long id) {
        log.info("<< Localizar por ID >>");
        Optional e = modeloRepository.findById(id);
        Entity e1 = (Entity) e.get();
        return (DTOSaida) e1.from();

    }


    @Transactional
    public void delete(Long id) {
        log.info("<< Excluir por ID >>");
        Optional e = modeloRepository.findById(id);
        Entity e1 = (Entity) e.get();
        modeloRepository.delete(e1);


    }


}
