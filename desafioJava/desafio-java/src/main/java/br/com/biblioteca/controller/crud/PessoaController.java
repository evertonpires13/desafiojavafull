package br.com.biblioteca.controller.crud;

import br.com.biblioteca.businnes.crud.PessoaBusinnes;
import br.com.biblioteca.dto.entrada.PessoaEntradaDTO;
import br.com.biblioteca.dto.saida.PessoaSaidaDTO;
import br.com.biblioteca.generic.GenericController;
import br.com.biblioteca.util.Mensagem;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

//@CrossOrigin
@Log4j2
@Controller
@RequestMapping("/v1/pessoa")
public class PessoaController extends GenericController<PessoaSaidaDTO, PessoaEntradaDTO, PessoaBusinnes> {
    @Override
    public String getDiretorio() {
        return "/v1/pessoa";
    }


    @PostMapping("/salvar")
    public String save(Model model, HttpServletRequest httpServletRequest
            , String cpf
            , String nascimento
            , String nome
            , String funcionario) {

        log.info(" funcionario => {}", funcionario);


        PessoaEntradaDTO pessoaEntradaDTO = PessoaEntradaDTO
                .builder()
                .cpf(cpf)
                .funcionario(funcionario)
                .nascimento(nascimento)
                .nome(nome)
                .build();

        PessoaSaidaDTO saida = businnes.save(pessoaEntradaDTO);


        if (saida != null && saida.getId() != null) {

            msg.add(Mensagem
                    .builder()
                    .codigo(Mensagem.SUCESSO)
                    .descricao("Pessoa salva com sucesso")
                    .build());
            model.addAttribute("mensagens", msg);
            model.addAttribute("lista", businnes.getAll());
            return getDiretorio() + "/listall";
        }


        model.addAttribute("modelo", pessoaEntradaDTO);
        return getDiretorio() + "/cadastro";
    }


}
