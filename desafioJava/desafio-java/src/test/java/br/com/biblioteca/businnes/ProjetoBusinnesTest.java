package br.com.biblioteca.businnes;

import br.com.biblioteca.businnes.crud.PessoaBusinnes;
import br.com.biblioteca.businnes.crud.ProjetoBusinnes;
import br.com.biblioteca.dto.entrada.ProjetoEntradaDTO;
import br.com.biblioteca.dto.saida.ProjetoSaidaDTO;
import br.com.biblioteca.stub.ProjetoEntradaDTOStub;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@Log4j2
@SpringBootTest
public class ProjetoBusinnesTest {

    @Autowired
    PessoaBusinnes pessoaBusinnes;

    @Autowired
    ProjetoBusinnes projetoBusinnes;



    @Test
    void testDeleteSuccessTest() {
        ProjetoEntradaDTO  projetoEntradaDTO = ProjetoEntradaDTOStub.getProjetoEntradaDTOGeneric();
        ProjetoSaidaDTO projetoSaidaDTO =  projetoBusinnes.save(projetoEntradaDTO);
        projetoBusinnes.delete(Long.parseLong( projetoSaidaDTO.getId()));
       // assertEquals(true, saida.getId()!=null);

    }


    @Test
    void testGetByIDSuccessTest() {
        ProjetoEntradaDTO  projetoEntradaDTO = ProjetoEntradaDTOStub.getProjetoEntradaDTOGeneric();
        ProjetoSaidaDTO projetoSaidaDTO =  projetoBusinnes.save(projetoEntradaDTO);
        ProjetoSaidaDTO saida =   projetoBusinnes.getID(Long.parseLong( projetoSaidaDTO.getId()));
        assertEquals(true, saida.getId()!=null);

    }

    @Test
    void testAllObjectsSuccessTest() {

        List<ProjetoSaidaDTO> lista =  projetoBusinnes.getAll();
        assertEquals(true, lista.size()>=0 );

    }
}
