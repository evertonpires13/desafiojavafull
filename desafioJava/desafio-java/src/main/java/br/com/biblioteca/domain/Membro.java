package br.com.biblioteca.domain;

import br.com.biblioteca.dto.saida.MembroSaidaDTO;
import br.com.biblioteca.generic.GenericDomain;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(name = "membros")
@Log4j2
@SuperBuilder
@NoArgsConstructor
public class Membro extends GenericDomain<MembroSaidaDTO> implements Serializable {

    @JoinColumn(name = "idprojeto", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Projeto projeto;


    @JoinColumn(name = "idpessoa", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Pessoa pessoa;


    @Column(name = "atribuicao", length = 80)
    private String atribuicao;


    @Override
    public MembroSaidaDTO from() {
        MembroSaidaDTO membroSaidaDTO = MembroSaidaDTO
                .builder()
                .id(getId().toString())
                .pessoaNome(pessoa.getNome())
                .projetoNome(projeto.getNome())
                .build();
        return membroSaidaDTO;
    }
}
