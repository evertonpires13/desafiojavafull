package br.com.biblioteca.controller;


import br.com.biblioteca.businnes.crud.MembroBusinnes;
import br.com.biblioteca.domain.Membro;
import br.com.biblioteca.util.Header;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@CrossOrigin
@Log4j2
@RestController
@RequestMapping("/v1/web/")
public class InserirMembroController {


    @Autowired
    private MembroBusinnes membroBusinnes;


    @GetMapping("insert/{nome}/{atribuicao}")
    public ResponseEntity<String> inserir(
            @Valid @PathVariable("nome") String nome
            , @Valid @PathVariable("projeto") String projeto
            , @Valid @PathVariable("cargo") String atribuicao
    ) {

        Membro membro = membroBusinnes.inserirMembro(nome, projeto, atribuicao);

        if (membro != null) {
            return Optional.ofNullable(new ResponseEntity<>("Sucesso", Header.getHeaders(), HttpStatus.OK)).orElse(new ResponseEntity<>(HttpStatus.BAD_GATEWAY));
        }
        return Optional.ofNullable(new ResponseEntity<>("Fracasso", Header.getHeaders(), HttpStatus.NOT_FOUND)).orElse(new ResponseEntity<>(HttpStatus.BAD_GATEWAY));

    }


    /*


    @GetMapping("/inserir/{cod_pessoa}/{cod_projeto}")
    public ResponseEntity<String> inserir(
            @Valid @PathVariable("cod_pessoa") String cod_pessoa
            , @Valid @PathVariable("cod_projeto") String cod_projeto) {

        Membro membro = membroBusinnes.inserirMembro(nome, atribuicao);

        if ( membro!=null) {
            return Optional.ofNullable(new ResponseEntity<>("Sucesso", Header.getHeaders(), HttpStatus.OK)).orElse(new ResponseEntity<>(HttpStatus.BAD_GATEWAY));
        }
        return Optional.ofNullable(new ResponseEntity<>("Fracasso", Header.getHeaders(), HttpStatus.NOT_FOUND)).orElse(new ResponseEntity<>(HttpStatus.BAD_GATEWAY));

    }

    */

}
