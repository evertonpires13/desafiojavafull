package br.com.biblioteca.businnes.crud;

import br.com.biblioteca.domain.Membro;
import br.com.biblioteca.domain.Pessoa;
import br.com.biblioteca.domain.Projeto;
import br.com.biblioteca.dto.entrada.MembroEntradaDTO;
import br.com.biblioteca.dto.saida.MembroSaidaDTO;
import br.com.biblioteca.generic.GenericBusinnes;
import br.com.biblioteca.repository.MembroRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class MembroBusinnes extends GenericBusinnes<MembroRepository, MembroSaidaDTO, MembroEntradaDTO, Membro> {


    @Autowired
    private PessoaBusinnes pessoaBusinnes;

    @Autowired
    private ProjetoBusinnes projetoBusinnes;


    public Membro inserirMembro(String nomePessoa, String nomeProjeto, String atribuicao) {

        Projeto projeto = projetoBusinnes.findByNome(nomePessoa);
        Pessoa pessoa = pessoaBusinnes.getByNome(nomeProjeto);

        if (projeto != null && projeto.getId() != null && pessoa != null && pessoa.getId() != null) {
            if (pessoa.getFuncionario() != null && pessoa.getFuncionario()) {
                Membro membro = Membro
                        .builder()
                        .projeto(projeto)
                        .pessoa(pessoa)
                        .atribuicao(atribuicao)
                        .build();
                membro = modeloRepository.save(membro);

                if (membro != null && membro.getId() != null) {
                    return membro;
                }
            }
        }

        return null;
    }
}
