package br.com.biblioteca.util;


import lombok.extern.log4j.Log4j2;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Log4j2
public class Utilitario {

    public static final Date convertToDate(String data) {

        try {
            Calendar dtNascimento = Calendar.getInstance();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            dtNascimento.setTime(sdf.parse(data));

            return dtNascimento.getTime();
        } catch (Exception e) {
            log.info(" Data Entrada => {}", data);
            log.info(" Erro na conversao de data => {}", e.toString());
            return null;
        }

    }


    public static final String convertToString(Date data) {

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.format(data);
        } catch (Exception e) {
            return null;
        }

    }
}
