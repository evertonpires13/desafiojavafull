package br.com.biblioteca.dto.entrada;

import br.com.biblioteca.domain.Pessoa;
import br.com.biblioteca.domain.Projeto;
import br.com.biblioteca.generic.GenericEntradaDTO;
import br.com.biblioteca.util.Utilitario;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Setter
@Builder
@Getter
public class ProjetoEntradaDTO extends GenericEntradaDTO<Projeto> {

    private String nome;

    private String dataInicio;

    private String dataPrevisaoFim;

    private String dataFim;

    private String descricao;

    private String orcamento;

    private String gerente;

    @Override
    public Projeto from() {

        Pessoa pessoa = Pessoa
                .builder()
                .id(Long.parseLong(gerente))
                .build();

        Projeto projeto = Projeto
                .builder()
                .gerente(pessoa)
                .dataFim(getDataFim() != null ? Utilitario.convertToDate(getDataFim()) : null)
                .dataInicio(getDataInicio() != null ? Utilitario.convertToDate(getDataInicio()) : null)
                .dataPrevisaoFim(getDataPrevisaoFim() != null ? Utilitario.convertToDate(getDataPrevisaoFim()) : null)
                .nome(getNome())
                .descricao(getDescricao())
                .orcamento(getOrcamento() != null ? Float.parseFloat(getOrcamento()) : null)
                .build();

        return projeto;
    }
}
