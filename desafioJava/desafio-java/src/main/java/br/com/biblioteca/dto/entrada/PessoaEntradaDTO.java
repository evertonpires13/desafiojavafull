package br.com.biblioteca.dto.entrada;

import br.com.biblioteca.domain.Pessoa;
import br.com.biblioteca.generic.GenericEntradaDTO;
import br.com.biblioteca.util.Utilitario;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Setter
@Builder
@Getter
public class PessoaEntradaDTO extends GenericEntradaDTO<Pessoa> {

    private String nome;
    private String cpf;

    private String nascimento;

    private String funcionario;

    @Override
    public Pessoa from() {

        return Pessoa
                .builder()
                .cpf(getCpf())
                .nome(getNome())
                .funcionario(getFuncionario() != null && getFuncionario().equalsIgnoreCase("s"))
                .dataNascimento(getNascimento() != null ? Utilitario.convertToDate(getNascimento()) : null)

                .build();
    }
}
