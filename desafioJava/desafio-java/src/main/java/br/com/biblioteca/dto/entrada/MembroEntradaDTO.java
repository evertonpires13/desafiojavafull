package br.com.biblioteca.dto.entrada;

import br.com.biblioteca.domain.Membro;
import br.com.biblioteca.domain.Pessoa;
import br.com.biblioteca.domain.Projeto;
import br.com.biblioteca.generic.GenericEntradaDTO;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Builder
@Getter
public class MembroEntradaDTO extends GenericEntradaDTO<Membro> {

    private String pessoa;

    private String projeto;


    @Override
    public Membro from() {

        Pessoa pessoa1 = Pessoa
                .builder()
                .id(Long.parseLong(pessoa))
                .build();

        Projeto projeto1 = Projeto
                .builder()
                .id(Long.parseLong(projeto))
                .build();


        Membro membro = Membro
                .builder()
                .pessoa(pessoa1)
                .projeto(projeto1)
                .build();
        return membro;
    }
}
