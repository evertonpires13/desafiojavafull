package br.com.biblioteca.generic;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@MappedSuperclass
@SuperBuilder
@NoArgsConstructor
public abstract class GenericDomain<DTO extends GenericSaidaDTO> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;


    public abstract DTO from();

}
