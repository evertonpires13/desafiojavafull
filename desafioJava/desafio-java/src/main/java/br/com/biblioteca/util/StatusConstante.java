package br.com.biblioteca.util;

public class StatusConstante {

    public static final Integer EM_ANALISE = 0;
    public static final Integer REALIZADA = 1;
    public static final Integer APROVADA = 2;
    public static final Integer INICIADO = 3;
    public static final Integer PLANEJADO = 4;
    public static final Integer ANDAMENTO = 5;
    public static final Integer ENCERRADO = 6;
    public static final Integer CANCELADO = 7;

}
