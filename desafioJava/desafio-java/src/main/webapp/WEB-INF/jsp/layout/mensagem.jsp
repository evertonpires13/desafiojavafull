

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<c:set var="sucesso" value="false"/>
<c:set var="erro" value="false"/>
<c:set var="atencao" value="false"/>


<c:forEach var="msg" items="${mensagens}">

    <c:if test="${msg.codigo==1}">
        <c:set var="sucesso" value="true"/>
    </c:if>

    <c:if test="${msg.codigo==2}">
        <c:set var="erro" value="true"/>
    </c:if>

    <c:if test="${msg.codigo==3}">
        <c:set var="atencao" value="true"/>
    </c:if>

</c:forEach>

<c:if test="${sucesso}">
    <div class="alert alert-success alert-dismissible" role="alert">
        <c:forEach var="msg" items="${mensagens}">
            <ul>
                <c:if test="${msg.codigo==1}">
                    <li>${msg.descricao}</li>
                </c:if>
            </ul>
        </c:forEach>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
</c:if>

<c:if test="${erro}">
    <div class="alert alert-danger alert-dismissible" role="alert">
        <c:forEach var="msg" items="${mensagens}">
            <ul>
                <c:if test="${msg.codigo==2}">
                    <li>${msg.descricao}</li>
                </c:if>
            </ul>
        </c:forEach>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
</c:if>

<c:if test="${atencao}">
    <div class="alert alert-warning alert-dismissible" role="alert">
        <c:forEach var="msg" items="${mensagens}">
            <ul>
                <c:if test="${msg.codigo==3}">
                    <li>${msg.descricao}</li>
                </c:if>
            </ul>
        </c:forEach>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
</c:if>
