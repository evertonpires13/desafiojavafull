package br.com.biblioteca.dto.saida;

import br.com.biblioteca.generic.GenericSaidaDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;


@Getter
@Setter
@Log4j2
@SuperBuilder
@NoArgsConstructor
public class PessoaSaidaDTO extends GenericSaidaDTO {


    private String nome;
    private String cpf;

    private String nascimento;

    private String funcionario;

}
