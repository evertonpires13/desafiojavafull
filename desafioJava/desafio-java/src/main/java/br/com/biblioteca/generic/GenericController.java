package br.com.biblioteca.generic;

import br.com.biblioteca.util.Mensagem;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public abstract class GenericController<
        DTOEntitySaida extends GenericSaidaDTO
        , DTOEntityEntrada extends GenericEntradaDTO
        , Businnes extends GenericBusinnes> {

    @Autowired
    protected Businnes businnes;
    protected List<Mensagem> msg = new ArrayList<>();

    public abstract String getDiretorio();

    @GetMapping("/list/all")
    public String listAll(Model model, HttpServletRequest httpServletRequest) {

        log.info(" << Gerando consulta de todos os registros >>");


        msg = new ArrayList<>();
       /*
        msg.add(Mensagem
                .builder()
                        .codigo(Mensagem.SUCESSO)
                        .descricao("Sucesso")
                .build());
        msg.add(Mensagem
                .builder()
                        .codigo(Mensagem.ERRO)
                        .descricao("deu erro")
                .build());
        */


        model.addAttribute("mensagens", msg);
        model.addAttribute("lista", businnes.getAll());
        return getDiretorio() + "/listall";
    }


    @GetMapping("/deletar/{id}")
    public String delete(Model model, HttpServletRequest httpServletRequest, @PathVariable String id) {

        if (id != null) {
            businnes.delete(Long.parseLong(id));
            msg.add(Mensagem.builder().codigo(Mensagem.SUCESSO).descricao("Excluido com sucesso").build());

        } else {
            msg.add(Mensagem.builder().codigo(Mensagem.SUCESSO).descricao("Identificador nao encontrado").build());
        }

        model.addAttribute("mensagens", msg);
        model.addAttribute("lista", businnes.getAll());
        return getDiretorio() + "/listall";
    }


    @GetMapping("/editar/{id}")
    public String getID(Model model, HttpServletRequest httpServletRequest, @PathVariable String id) {

        if (id != null) {
            Long g = Long.parseLong(id);
            GenericSaidaDTO saida = businnes.getID(g);
            model.addAttribute("modelo", saida);
            log.info(" registro => {} ", saida.getId());
        } else {
            msg.add(Mensagem
                    .builder()
                    .codigo(Mensagem.ERRO)
                    .descricao("Identificador nao encontrado")
                    .build());
        }
        model.addAttribute("mensagens", msg);
        return getDiretorio() + "/cadastro";
    }


    @GetMapping("/novo")
    public String novo(Model model, HttpServletRequest httpServletRequest) {

        //  Pessoa p =Pessoa.builder().cpf("123").build();
        //  model.addAttribute("modelo",p);
        model.addAttribute("mensagens", msg);
        return getDiretorio() + "/cadastro";
    }

}
