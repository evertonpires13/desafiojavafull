package br.com.biblioteca.businnes;

import br.com.biblioteca.businnes.crud.MembroBusinnes;
import br.com.biblioteca.businnes.crud.PessoaBusinnes;
import br.com.biblioteca.businnes.crud.ProjetoBusinnes;
import br.com.biblioteca.domain.Membro;
import static org.junit.jupiter.api.Assertions.assertEquals;
import br.com.biblioteca.dto.saida.PessoaSaidaDTO;
import br.com.biblioteca.dto.saida.ProjetoSaidaDTO;
import br.com.biblioteca.stub.PessoaEntradaDTOStub;
import br.com.biblioteca.stub.ProjetoEntradaDTOStub;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import lombok.extern.log4j.Log4j2;

@Log4j2
@SpringBootTest
public class MembroBusinnesTest {

    @Autowired
    MembroBusinnes membroBusinnes;

    @Autowired
    PessoaBusinnes pessoaBusinnes;


    @Autowired
    ProjetoBusinnes projetoBusinnes;


    @Test
    void testInsertMembroSuccessTest() {

        ProjetoSaidaDTO projetoSaidaDTO =  projetoBusinnes.save(ProjetoEntradaDTOStub.getProjetoEntradaDTOGeneric());
        PessoaSaidaDTO pessoaSaidaDTO = pessoaBusinnes.save(PessoaEntradaDTOStub.getPessoaEntradaDTOGeneric());

        Membro membro =   membroBusinnes.inserirMembro(pessoaSaidaDTO.getNome()  , projetoSaidaDTO.getNome(),"gerente");
        assertEquals(true, membro.getId()!=null);

    }

}
