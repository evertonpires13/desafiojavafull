package br.com.biblioteca.controller.crud;

import br.com.biblioteca.businnes.crud.MembroBusinnes;
import br.com.biblioteca.businnes.crud.PessoaBusinnes;
import br.com.biblioteca.businnes.crud.ProjetoBusinnes;
import br.com.biblioteca.dto.entrada.MembroEntradaDTO;
import br.com.biblioteca.dto.saida.MembroSaidaDTO;
import br.com.biblioteca.generic.GenericController;
import br.com.biblioteca.util.Mensagem;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Log4j2
@Controller
@RequestMapping("/v1/membro")
public class MembroController extends GenericController<MembroSaidaDTO, MembroEntradaDTO, MembroBusinnes> {

    @Autowired
    private PessoaBusinnes pessoaBusinnes;
    @Autowired
    private ProjetoBusinnes projetoBusinnes;

    @Override
    public String getDiretorio() {
        return "/v1/membro";
    }

    @PostMapping("/salvar")
    public String save(Model model, HttpServletRequest httpServletRequest
            , String pessoa
            , String projeto) {

        // PessoaSaidaDTO pessoaSaidaDTO =  pessoaBusinnes.getID(Long.parseLong(pessoa));
        //  ProjetoSaidaDTO projetoSaidaDTO =     projetoBusinnes.getID(Long.parseLong(projeto));


        MembroEntradaDTO membroEntradaDTO = MembroEntradaDTO
                .builder()
                .pessoa(pessoa)
                .projeto(projeto)
                .build();

        MembroSaidaDTO saida = businnes.save(membroEntradaDTO);


        if (saida != null && saida.getId() != null) {

            msg.add(Mensagem
                    .builder()
                    .codigo(Mensagem.SUCESSO)
                    .descricao("Membro salvo com sucesso")
                    .build());
            model.addAttribute("mensagens", msg);
            model.addAttribute("lista", businnes.getAll());
            return getDiretorio() + "/listall";
        }

        model.addAttribute("mensagens", msg);
        model.addAttribute("modelo", membroEntradaDTO);
        return getDiretorio() + "/cadastro";
    }

}
