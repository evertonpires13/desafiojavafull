package br.com.biblioteca.domain;

import br.com.biblioteca.dto.saida.ProjetoSaidaDTO;
import br.com.biblioteca.generic.GenericDomain;
import br.com.biblioteca.util.Utilitario;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "projeto")
@Log4j2
@SuperBuilder
@NoArgsConstructor
public class Projeto extends GenericDomain<ProjetoSaidaDTO> implements Serializable {

    @Column(name = "nome", length = 200)
    private String nome;

    @Column(name = "data_inicio")
    private Date dataInicio;

    @Column(name = "data_previsao_fim")
    private Date dataPrevisaoFim;

    @Column(name = "data_fim")
    private Date dataFim;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "status", length = 45)
    private Integer status;

    @Column(name = "orcamento")
    private Float orcamento;

    @Column(name = "risco", length = 45)
    private Integer risco;

    @JoinColumn(name = "idgerente", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Pessoa gerente;

    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE}, mappedBy = "projeto" /*, orphanRemoval = true */)
    private List<Membro> membros;


    @Override
    public ProjetoSaidaDTO from() {

        ProjetoSaidaDTO projetoSaidaDTO = ProjetoSaidaDTO
                .builder()
                .descricao(getDescricao())
                .status(getStatus())
                .nome(getNome())
                .risco(getRisco())
                .id(getId().toString())
                .gerente(getGerente() != null && getGerente().getId() != null ? getGerente().getNome() : "")
                .orcamento(getOrcamento() != null ? getOrcamento().toString() : "")
                .dataPrevisaoFim(getDataPrevisaoFim() != null ? Utilitario.convertToString(getDataPrevisaoFim()) : "")
                .dataInicio(getDataInicio() != null ? Utilitario.convertToString(getDataInicio()) : "")
                .dataFim(getDataFim() != null ? Utilitario.convertToString(getDataFim()) : "")
                .build();
        return projetoSaidaDTO;
    }
}
