package br.com.biblioteca.businnes.crud;

import br.com.biblioteca.domain.Pessoa;
import br.com.biblioteca.dto.entrada.PessoaEntradaDTO;
import br.com.biblioteca.dto.saida.PessoaSaidaDTO;
import br.com.biblioteca.generic.GenericBusinnes;
import br.com.biblioteca.repository.PessoaRepository;
import org.springframework.stereotype.Component;

@Component
public class PessoaBusinnes extends GenericBusinnes<PessoaRepository, PessoaSaidaDTO, PessoaEntradaDTO, Pessoa> {

    public Pessoa getByNome(String nome) {
        return modeloRepository.findTop1ByNome(nome);
    }

}
