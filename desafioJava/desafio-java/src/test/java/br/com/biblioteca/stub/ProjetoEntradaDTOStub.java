package br.com.biblioteca.stub;


import br.com.biblioteca.dto.entrada.ProjetoEntradaDTO;

import java.util.Calendar;

public class ProjetoEntradaDTOStub {


    public static final ProjetoEntradaDTO getProjetoEntradaDTOGeneric(){

        Calendar calendar = Calendar.getInstance();
        ProjetoEntradaDTO projetoEntradaDTO = ProjetoEntradaDTO
                .builder()
                .nome("Projeto Principal " + calendar.getTimeInMillis())
                .gerente("1")
                .orcamento("500")
                .dataInicio("2023-01-01")
                .dataPrevisaoFim("2023-10-05")
                .dataFim("2024-01-01")
                .descricao("Descricao do projeto")
                .build();

        return  projetoEntradaDTO;

    }
}
