<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>





            <!-- Footer -->
            <footer class="content-footer footer bg-footer-theme">
              <div class="container-xxl d-flex flex-wrap justify-content-between py-2 flex-md-row flex-column">
                <div class="mb-2 mb-md-0">
                  ©
                  <script>
                    document.write(new Date().getFullYear());
                  </script>
                  , Desafio Java️ by
                  <a href="#" target="_blank" class="footer-link fw-bolder">Everton Pires</a>
                </div>

              </div>
            </footer>
            <!-- / Footer -->

            <div class="content-backdrop fade"></div>
          </div>
          <!-- Content wrapper -->
        </div>
        <!-- / Layout page -->
      </div>

      <!-- Overlay -->
      <div class="layout-overlay layout-menu-toggle"></div>
    </div>
    <!-- / Layout wrapper -->


<!--
    <div class="buy-now">
      <a href="https://themeselection.com/products/sneat-bootstrap-html-admin-template/"
        target="_blank"
        class="btn btn-danger btn-buy-now" >Upgrade to Pro</a>
    </div>
    -->
 
    <script src="<c:url value="/assets/vendor/libs/jquery/jquery.js"/>"></script>
    <script src="<c:url value="/assets/vendor/libs/popper/popper.js"/>"></script>
    <script src="<c:url value="/assets/vendor/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"/>"></script>

    <script src="<c:url value="/assets/vendor/js/menu.js"/>"></script>

    <script src="<c:url value="/assets/js/main.js"/>"></script>

    <script src="<c:url value="/assets/js/form-basic-inputs.js"/>"></script>
    <script async defer src="https://buttons.github.io/buttons.js"/>"></script>


  </body>
</html>