package br.com.biblioteca.generic;

import lombok.Getter;

@Getter
public abstract class GenericEntradaDTO<T extends GenericDomain> {

    private String id;

    public abstract T from();

}
