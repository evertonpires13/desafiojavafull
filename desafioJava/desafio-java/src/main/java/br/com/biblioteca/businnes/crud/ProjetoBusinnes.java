package br.com.biblioteca.businnes.crud;

import br.com.biblioteca.domain.Projeto;
import br.com.biblioteca.dto.entrada.ProjetoEntradaDTO;
import br.com.biblioteca.dto.saida.ProjetoSaidaDTO;
import br.com.biblioteca.generic.GenericBusinnes;
import br.com.biblioteca.repository.ProjetoRepository;
import br.com.biblioteca.util.RiscoConstante;
import br.com.biblioteca.util.StatusConstante;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Log4j2
@Component
public class ProjetoBusinnes extends GenericBusinnes<ProjetoRepository, ProjetoSaidaDTO, ProjetoEntradaDTO, Projeto> {

    public Projeto findByNome(String nome) {
        return modeloRepository.findTop1ByNome(nome);
    }

    public void alterarStatus(Long idProjeto, Integer status) {

        Projeto projeto = modeloRepository.getById(idProjeto);
        projeto.setStatus(status);
        modeloRepository.save(projeto);

    }

    public void alterarRisco(Long idProjeto, Integer risco) {
        Projeto projeto = modeloRepository.getById(idProjeto);
        projeto.setRisco(risco);
        modeloRepository.save(projeto);

    }


    public synchronized void delete(Long id) {
        log.info("<< Excluir por ID >>");
        Optional e = modeloRepository.findById(id);
        Projeto projeto = (Projeto) e.get();

        if (projeto.getStatus() != StatusConstante.INICIADO && projeto.getStatus() != StatusConstante.ANDAMENTO
                && projeto.getStatus() != StatusConstante.ENCERRADO) {
            modeloRepository.delete(projeto);
        }


    }

    public synchronized ProjetoSaidaDTO save(ProjetoEntradaDTO entity) {

        log.info("<< Iniciar processo de salvar generico >>");

        try {
            boolean registroNovo = entity != null && entity.getId() == null;

            Projeto e = entity.from();
            e = modeloRepository.save(e);

            if (e != null && e.getId() != null && registroNovo) {
              //  alterarStatus(e.getId(), RiscoConstante.BAIXO);
              //  alterarRisco(e.getId(), StatusConstante.EM_ANALISE);
            }
            return e.from();
        }catch(Exception e ){
          log.info("Erro ao salvar projeto => {}", e.toString());
        }

        return null;

    }

}
