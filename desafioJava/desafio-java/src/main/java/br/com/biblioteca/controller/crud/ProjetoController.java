package br.com.biblioteca.controller.crud;

import br.com.biblioteca.businnes.crud.ProjetoBusinnes;
import br.com.biblioteca.dto.entrada.ProjetoEntradaDTO;
import br.com.biblioteca.dto.saida.ProjetoSaidaDTO;
import br.com.biblioteca.generic.GenericController;
import br.com.biblioteca.util.Mensagem;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Log4j2
@Controller
@RequestMapping("/v1/projeto")
public class ProjetoController extends GenericController<ProjetoSaidaDTO, ProjetoEntradaDTO, ProjetoBusinnes> {


    @Override
    public String getDiretorio() {
        return "/v1/projeto";
    }

    @PostMapping("/salvar")
    public String save(Model model, HttpServletRequest httpServletRequest
            , String nome
            , String dataInicio
            , String dataPrevisaoFim
            , String dataFim
            , String descricao
                       //  ,  String status
            , String orcamento
                       // ,String risco
            , String gerente) {


        ProjetoEntradaDTO projetoEntradaDTO = ProjetoEntradaDTO
                .builder()
                .nome(nome)
                .dataFim(dataFim)
                .dataInicio(dataInicio)
                .nome(nome)
                .dataPrevisaoFim(dataPrevisaoFim)
                .descricao(descricao)
                //.status(status)
                .orcamento(orcamento)
                //  .risco(risco)
                .gerente(gerente)
                .build();

        ProjetoSaidaDTO saida = businnes.save(projetoEntradaDTO);

        if (saida != null && saida.getId() != null) {
            msg.add(Mensagem.builder().codigo(Mensagem.SUCESSO).descricao("Projeto salvo com sucesso").build());
            model.addAttribute("mensagens", msg);
            model.addAttribute("lista", businnes.getAll());
            return getDiretorio() + "/listall";
        }

        model.addAttribute("mensagens", msg);
        model.addAttribute("modelo", projetoEntradaDTO);
        return getDiretorio() + "/cadastro";
    }

}
