package br.com.biblioteca.util;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class Header {
    public static final String HEADER_TRAVELS_API_VERSION = "travels-api-version";

    public static final String HEADER_API_KEY = "X-api-key";

    private static final String apiVersion = "1.0";
    private static final String apiKey = "1.0";

    public static MultiValueMap<String, String> getHeaders() {

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HEADER_TRAVELS_API_VERSION, apiVersion);
        headers.add(HEADER_API_KEY, apiKey);
        return headers;
    }

}
