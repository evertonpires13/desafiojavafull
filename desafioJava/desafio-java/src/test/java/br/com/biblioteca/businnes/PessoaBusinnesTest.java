package br.com.biblioteca.businnes;

import br.com.biblioteca.businnes.crud.PessoaBusinnes;
import br.com.biblioteca.domain.Pessoa;
import br.com.biblioteca.dto.entrada.PessoaEntradaDTO;
import br.com.biblioteca.dto.saida.PessoaSaidaDTO;
import br.com.biblioteca.stub.PessoaEntradaDTOStub;
import static org.junit.jupiter.api.Assertions.assertEquals;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


@Log4j2
@SpringBootTest
public class PessoaBusinnesTest {


    @Autowired
    PessoaBusinnes pessoaBusinnes;


    @Test
    void testDeleteSuccessTest() {
        PessoaEntradaDTO pessoaEntradaDTO = PessoaEntradaDTOStub.getPessoaEntradaDTOGeneric();
        PessoaSaidaDTO pessoaSaidaDTO =  pessoaBusinnes.save(pessoaEntradaDTO);
        pessoaBusinnes.delete(Long.parseLong( pessoaSaidaDTO.getId()));

    }

    @Test
    void testGetByIDSuccessTest() {
        PessoaEntradaDTO pessoaEntradaDTO = PessoaEntradaDTOStub.getPessoaEntradaDTOGeneric();
        PessoaSaidaDTO pessoaSaidaDTO =  pessoaBusinnes.save(pessoaEntradaDTO);
        PessoaSaidaDTO saida =   pessoaBusinnes.getID(Long.parseLong( pessoaSaidaDTO.getId()));
        assertEquals(true, saida.getId()!=null);

    }

    @Test
    void testGetByNameSuccessTest() {
        PessoaEntradaDTO pessoaEntradaDTO = PessoaEntradaDTOStub.getPessoaEntradaDTOGeneric();
        PessoaSaidaDTO pessoaSaidaDTO =  pessoaBusinnes.save(pessoaEntradaDTO);
        Pessoa saida =   pessoaBusinnes.getByNome( pessoaSaidaDTO.getNome());
        assertEquals(true, saida.getId()!=null);
    }


    @Test
    void testAllObjectsSuccessTest() {

        List<PessoaSaidaDTO> lista =  pessoaBusinnes.getAll();
        assertEquals(true, lista.size()>=0 );

    }

}