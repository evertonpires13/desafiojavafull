
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<jsp:include page="../../layout/head.jsp" />
 
<!-- Content -->

<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Cadastros /</span> Projetos</h4>

    <div class="row">


        <hr class="my-5" />
        <jsp:include page="../../layout/mensagem.jsp" />

        <!-- Bootstrap Table with Header - Light -->
        <div class="card">
            <h5 class="card-header">Lista de Projetos</h5>
            <div class="table-responsive text-nowrap">
                <table class="table">
                    <thead class="table-light">
                        <tr>
                            <th>Nome</th>
                            <th>Inicio</th>
                            <th>Fim</th>
                            <th>Previsao Fim</th>
                             <th>Editar</th>
                            <th>Excluir</th>
                        </tr>
                    </thead>

                    <tbody class="table-border-bottom-0">

                        <c:forEach var="registro" items="${lista}">   

                            <tr>
                                <td>
                                    <i class="fab fa-angular fa-lg text-danger me-3"></i> 
                                    <strong> ${registro.nome}</strong>
                                </td>
                                <td>${registro.dataInicio}</td>
                                <td>${registro.dataFim}  </td>
                                <td>${registro.dataPrevisaoFim}  </td>
 
                                 <td>
                                    <a class="btn btn-icon btn-primary" href="/v1/projeto/editar/${registro.id}">
                                        <span class="tf-icons bx bx-edit-alt"></span>
                                    </a>
                                </td>
                                
                                <td>
                                     <a class="btn btn-icon btn-primary" href="/v1/projeto/deletar/${registro.id}">
                                        <span class="tf-icons bx bx-trash"></span>
                                    </a>
                                </td>
                            </tr>

                        </c:forEach>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- Bootstrap Table with Header - Light -->

        <div class="col-xl-6">  </div>
    </div>
</div>
<!-- / Content -->




<div class="buy-now">
    <a href="/v1/projeto/novo"
       class="btn btn-danger btn-buy-now" >Cadastrar Projeto</a>
</div>

<jsp:include page="../../layout/footer.jsp" />