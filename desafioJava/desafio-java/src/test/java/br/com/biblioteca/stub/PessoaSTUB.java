package br.com.biblioteca.stub;

import br.com.biblioteca.domain.Pessoa;

public class PessoaSTUB {


    public static final Pessoa pessoaGeneric(){

        Pessoa pessoa = Pessoa
                .builder()
                .nome("Joao")
                .cpf("11111111111")
                .funcionario(true)
                .build();

        return pessoa;
    }
}
