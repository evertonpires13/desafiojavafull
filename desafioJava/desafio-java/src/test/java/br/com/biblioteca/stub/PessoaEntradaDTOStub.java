package br.com.biblioteca.stub;

import br.com.biblioteca.dto.entrada.PessoaEntradaDTO;

import java.util.Calendar;

public class PessoaEntradaDTOStub {

    public static final PessoaEntradaDTO getPessoaEntradaDTOGeneric(){

        Calendar calendar = Calendar.getInstance();
        PessoaEntradaDTO pessoaEntradaDTO = PessoaEntradaDTO
                .builder()
                .nome("Sizenando Silva " + calendar.getTimeInMillis())
                .nascimento("2010-10-13")
                .cpf("11111111111")
                .funcionario("s")
                .build();

        return  pessoaEntradaDTO;

    }
 }
