package br.com.biblioteca.repository;

import br.com.biblioteca.domain.Projeto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProjetoRepository extends JpaRepository<Projeto, Long> {


    public Projeto findTop1ByNome(String nome);




}
