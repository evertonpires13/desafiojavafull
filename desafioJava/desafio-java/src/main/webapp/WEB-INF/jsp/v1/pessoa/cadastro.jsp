
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<jsp:include page="../../layout/head.jsp" />
 
<!-- Content -->
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Cadastros /</span> Pessoa</h4>

    <div class="row">
        
         
            <jsp:include page="../../layout/mensagem.jsp" />
         <form method="post" action="/v1/pessoa/salvar" >
             
            <input name="id" type="hidden" class="form-control" value="${modelo.id}"/>


            <!-- Merged -->
            <div class="col-md-12">

                <div class="card mb-12">
                    <h5 class="card-header">Pessoa</h5>
                    <div class="card-body demo-vertical-spacing demo-only-element">

                        <div class="form-password-toggle col-md-6">
                            <label class="form-label" for="basic-default-password32">Nome</label>
                            <div class="input-group input-group-merge">
                                <input name="nome"  type="input" class="form-control"  value="${modelo.nome}"/>
                                <span class="input-group-text cursor-pointer" id="basic-default-password"><i class="bx bx-hide"></i></span>
                            </div>
                        </div>

                        <div class="form-password-toggle  col-md-6">
                            <label class="form-label" for="basic-default-password32">CPF</label>
                            <div class="input-group input-group-merge">
                                <input name="cpf" type="input" class="form-control" value="${modelo.cpf}"/>
                                <span class="input-group-text cursor-pointer" ><i class="bx bx-hide"></i></span>
                            </div>
                        </div> 

                         <div class="form-password-toggle col-md-6">
                            <label for="html5-date-input" class="col-md-2 col-form-label">Nascimento</label>
                            <div class="col-md-10">
                                <input class="form-control" type="date" name="nascimento" value="${modelo.nascimento}"/>
                            </div>
                        </div>

                         <div class="form-password-toggle col-md-6">
                             <input class="form-check-input" type="checkbox" value="s"  name="funcionario" ${modelo.funcionario}/>
                            <label class="form-check-label" for="defaultCheck1"> Funcionario </label>
                        </div>

                    </div>

                    <div class="card-body">
                        <small class="text-light fw-semibold">Salvar Dados</small>
                        <div class="demo-inline-spacing">
                            <input type="submit" class="btn btn-primary" value="Salvar Dados"/>
                        </div>
                    </div>

                </div>

            </div>
        </form>
   
                                
                                
                                
        <div class="col-xl-6">  </div>
    </div>
</div>
<!-- / Content -->


<jsp:include page="../../layout/footer.jsp" />