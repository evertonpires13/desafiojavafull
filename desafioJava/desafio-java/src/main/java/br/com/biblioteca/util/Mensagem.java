package br.com.biblioteca.util;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Mensagem {

    public static final Integer SUCESSO = 1;
    public static final Integer ERRO = 2;
    public static final Integer ATENCAO = 3;

    private Integer codigo;
    private String descricao;

}
