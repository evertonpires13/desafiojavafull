package br.com.biblioteca.domain;

import br.com.biblioteca.dto.saida.PessoaSaidaDTO;
import br.com.biblioteca.generic.GenericDomain;
import br.com.biblioteca.util.Utilitario;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "pessoa")
@Log4j2
@SuperBuilder
@NoArgsConstructor
public class Pessoa extends GenericDomain<PessoaSaidaDTO> implements Serializable {

    @Column(name = "nome", length = 100, unique = true, nullable = false)
    private String nome;


    @Column(name = "datanascimento")
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;

    @Column(name = "cpf", length = 14)
    private String cpf;

    @Column(name = "funcionario")
    private Boolean funcionario;


    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE}, mappedBy = "gerente" /*, orphanRemoval = true */)
    private List<Projeto> projetos;


    @OneToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE}, mappedBy = "pessoa" /*, orphanRemoval = true */)
    private List<Membro> membros;


    @Override
    public PessoaSaidaDTO from() {

        PessoaSaidaDTO ps = PessoaSaidaDTO
                .builder()
                .cpf(getCpf())
                .id(getId().toString())
                .nome(getNome())
                .funcionario(getFuncionario() != null && getFuncionario() ? "checked" : "")
                .nascimento(getDataNascimento() != null ? Utilitario.convertToString(getDataNascimento()) : "")
                .build();
        return ps;
    }
}
