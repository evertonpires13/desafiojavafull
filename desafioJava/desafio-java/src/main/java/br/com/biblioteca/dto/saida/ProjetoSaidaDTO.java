package br.com.biblioteca.dto.saida;

import br.com.biblioteca.generic.GenericSaidaDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.log4j.Log4j2;

@Getter
@Setter
@Log4j2
@SuperBuilder
@NoArgsConstructor
public class ProjetoSaidaDTO extends GenericSaidaDTO {


    private String nome;

    private String dataInicio;

    private String dataPrevisaoFim;

    private String dataFim;

    private String descricao;

    private Integer status;

    private String orcamento;

    private Integer risco;

    private String gerente;


}
