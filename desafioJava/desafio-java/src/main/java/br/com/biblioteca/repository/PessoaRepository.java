package br.com.biblioteca.repository;

import br.com.biblioteca.domain.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

    public Pessoa findTop1ByNome(String nome);

}
